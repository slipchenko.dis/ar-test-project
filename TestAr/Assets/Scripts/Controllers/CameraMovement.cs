using UnityEngine;
using UnityEngine.XR.ARFoundation;
using ARTest.UI;

namespace ARTest.Controllers
{
    public class CameraMovement : MonoBehaviour
    {
        [SerializeField] float _moveSpeed;
        [SerializeField] float _directionMultiplayer;
        [SerializeField] ARSessionOrigin _arSection;
        [SerializeField] UIJoystickController _joystickController;

        Vector3 _cameraPosition;

        void Start()
        {
            _cameraPosition = transform.position;

            _joystickController.MoveUp += MoveUp;
            _joystickController.MoveLeft += MoveLeft;
            _joystickController.MoveRight += MoveRight;
            _joystickController.MoveDown += MoveDown;
            _joystickController.MoveForward += MoveForward;
            _joystickController.MoveBackward += MoveBackward;
        }

        void Update()
        {
            float step = _moveSpeed * Time.deltaTime;

            var newPosition = Vector3.MoveTowards(transform.position, _cameraPosition, step);
            transform.position = newPosition;
        }

        void MoveUp()
        {
            _cameraPosition += _arSection.camera.transform.transform.up * _directionMultiplayer;
        }

        void MoveLeft()
        {
            _cameraPosition += -_arSection.camera.transform.transform.right * _directionMultiplayer;
        }

        void MoveRight()
        {
            _cameraPosition += _arSection.camera.transform.transform.right * _directionMultiplayer;
        }

        void MoveDown()
        {
            _cameraPosition += -_arSection.camera.transform.transform.up * _directionMultiplayer;
        }

        void MoveForward()
        {
            _cameraPosition += _arSection.camera.transform.transform.forward * _directionMultiplayer;
        }

        void MoveBackward()
        {
            _cameraPosition += -_arSection.camera.transform.transform.forward * _directionMultiplayer;
        }
    }
}

