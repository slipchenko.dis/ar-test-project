using System;
using UnityEngine;

namespace ARTest.UI
{
    public class UIJoystickController : MonoBehaviour
    {
        #region Singleton
        public static UIJoystickController Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        public Action MoveUp;
        public Action MoveLeft;
        public Action MoveRight;
        public Action MoveDown;
        public Action MoveForward;
        public Action MoveBackward;

        // on pointer up = false / down = true;
        bool left, right, up, down, forward, backward;

        private void FixedUpdate()
        {
            Movement();
        }

        void Movement()
        {
            if (up)
                MoveUp?.Invoke();
            if (left)
                MoveLeft?.Invoke();
            if (down)
                MoveDown?.Invoke();
            if (right)
                MoveRight?.Invoke();
            if (forward)
                MoveForward?.Invoke();
            if (backward)
                MoveBackward?.Invoke();
        }

        public void UpPressed(bool value)
        {
            up = value;
        }

        public void LeftPressed(bool value)
        {
            left = value;
        }

        public void RightPressed(bool value)
        {
            right = value;
        }

        public void DownPressed(bool value)
        {
            down = value;
        }

        public void ForwardPressed(bool value)
        {
            forward = value;
        }

        public void BackwardPressed(bool value)
        {
            backward = value;
        }
    }
}

