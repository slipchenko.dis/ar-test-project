using System;
using UnityEngine;
using ARTest.AssetsBundle;

namespace ARTest.UI
{
    public class UIController : MonoBehaviour
    {
        #region Singleton
        public static UIController Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        public Action OnStartPressed;

        [SerializeField] GameObject _menuPanel;
        [SerializeField] GameObject _gameplayPanel;
        [SerializeField] GameObject _loadingPanel;

        private void Start()
        {
            AssetBundleReader.Instance.OnReadComplete += OnReadComplete;
        }

        // on start button pressed
        public void StartGame()
        {
            _menuPanel.SetActive(false);
            _gameplayPanel.SetActive(true);

            OnStartPressed?.Invoke();
        }

        public void ShowLoadingPanel()
        {
            _loadingPanel.SetActive(true);
        }

        private void OnReadComplete(AssetBundle assetBundle)
        {
            _loadingPanel.SetActive(false);
        }
    }
}

