using System;
using System.Collections;
using UnityEngine;
using ARTest.UI;

namespace ARTest.AssetsBundle
{
    public class AssetBundleReader : MonoBehaviour
    {
        #region Singleton
        public static AssetBundleReader Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        public event Action<AssetBundle> OnReadComplete;

        [Header("Android Assets")]
        [SerializeField] string _androidAssetName;
        [Header("IOS Assets")]
        [SerializeField] string _iosAssetName;

        AssetBundle _assetBundle;

        private void Start()
        {
            UIController.Instance.OnStartPressed += Read;
        }

        public void Read()
        {
            StartCoroutine(ReadAssets());
        }

        IEnumerator ReadAssets()
        {
            UIController.Instance.ShowLoadingPanel();

#if UNITY_ANDROID || UNITY_EDITOR
            var request = WWW.LoadFromCacheOrDownload(Application.streamingAssetsPath + "/" + _androidAssetName, 0);
#elif UNITY_IOS
        var request = WWW.LoadFromCacheOrDownload(Application.streamingAssetsPath + "/" + _iosAssetName, 0);
#endif

            // loading
            while (!request.isDone)
            {
                Debug.Log(request.progress);
                yield return null;
            }

            if (request.error == null)
            {
                Debug.Log("SUCCESS!");
            }
            else
            {
                Debug.Log(request.error);
            }

            _assetBundle = request.assetBundle;

            OnReadComplete?.Invoke(_assetBundle);
        }
    }
}

