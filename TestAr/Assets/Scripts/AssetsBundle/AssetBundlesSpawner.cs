using UnityEngine;

namespace ARTest.AssetsBundle
{
    public class AssetBundlesSpawner : MonoBehaviour
    {
        private void Start()
        {
            AssetBundleReader.Instance.OnReadComplete += SpawnAllAssets;
        }

        // spawn after reading assetbundle
        private void SpawnAllAssets(AssetBundle assetBundle)
        {
            if (assetBundle == null) return;

            var assetsObjects = assetBundle.LoadAllAssets<GameObject>();

            foreach (var asset in assetsObjects)
            {
                var newObject = Instantiate(asset);
                newObject.transform.position = Camera.main.transform.position;
            }
        }
    }
}

